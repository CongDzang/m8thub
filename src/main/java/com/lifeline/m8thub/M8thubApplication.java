package com.lifeline.m8thub;

import java.util.List;

import org.red5.server.adapter.MultiThreadedApplicationAdapter;
import org.red5.server.api.IConnection;
import org.red5.server.api.scope.IScope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.datastax.driver.core.Session;
import com.lifeline.m8thub.connection.CassandraConnector;
import com.lifeline.m8thub.entity.Customer;
import com.lifeline.m8thub.service.CustomerService;

/**
 * @author daccattato
 *
 */
public class M8thubApplication extends MultiThreadedApplicationAdapter {
	private static final Logger LOG = LoggerFactory.getLogger(M8thubApplication.class);
	
	@Autowired
	private CustomerService customerService;
	
	@Override
	public boolean appConnect(IConnection conn, Object[] params) {
		log.info("appConnect");
		testCustomerService();
		return super.appConnect(conn, params);
	}

	@Override
	public void appDisconnect(IConnection conn) {
		log.info("appDisconnect");
		super.appDisconnect(conn);
	}

	@Override
	public boolean appStart(IScope app) {
		log.info("appStart");
		testConnectionWithCassandra();
	//	testCustomerService();
		return super.appStart(app);
	}
	
	public void testConnectionWithCassandra() {
		CassandraConnector connector = new CassandraConnector();
		connector.connect("127.0.0.1", null);
		
		Session session = connector.getSession();

//        KeyspaceRepository sr = new KeyspaceRepository(session);
//        sr.createKeyspace("library", "SimpleStrategy", 1);
//        sr.useKeyspace("library");
//        
//        BookRepository br = new BookRepository(session);
//        br.createTable();
//        br.alterTablebooks("publisher", "text");
//
//        br.createTableBooksByTitle();
//
//        Book book = new Book(UUIDs.timeBased(), "Effective Java", "Joshua Bloch", "Programming");
//        br.insertBookBatch(book);
//
//        br.selectAll().forEach(o -> LOG.info("Title in books: " + o.getTitle()));
//        br.selectAllBookByTitle().forEach(o -> LOG.info("Title in booksByTitle: " + o.getTitle()));

//        br.deletebookByTitle("Effective Java");
//        br.deleteTable("books");
//        br.deleteTable("booksByTitle");
//
//        sr.deleteKeyspace("library");

        connector.close();
	}
	
	public  void testCustomerService() {
		List<Customer> list = customerService.findAll();
		log.info("customer service works");
		
		Customer cust = new Customer(4l, "url", "0112332", "Chau Phi");
		customerService.save(cust);
		
	}


}
