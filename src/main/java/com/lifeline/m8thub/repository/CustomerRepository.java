package com.lifeline.m8thub.repository;

import com.lifeline.m8thub.entity.Customer;

public interface CustomerRepository extends IRepository<Customer>{

}
