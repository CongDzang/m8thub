package com.lifeline.m8thub.repository;
import org.springframework.data.jpa.domain.AbstractPersistable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IRepository<T extends AbstractPersistable<Long>> extends JpaRepository<T, Long>, JpaSpecificationExecutor<T> {

}

