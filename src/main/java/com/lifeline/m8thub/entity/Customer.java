package com.lifeline.m8thub.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.AbstractPersistable;

@Entity
@Table(name="customers")
public class Customer extends AbstractPersistable<Long> {
 
	/** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;
  
  @Column(name = "user_id")
  private Long userId;
  
  @Column(name = "avatar")
  private String avatar;
  
  @Column(name = "phone")
  private String phone;
  
  @Column(name = "address")
  private String address;
  
  
  public Customer() {
    super();
  }

  public Customer(Long userId, String avatar, String phone, String address) {
    this.userId = userId;
    this.avatar = avatar;
    this.phone = phone;
    this.address = address;

  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getAvatar() {
    return avatar;
  }

  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

}
