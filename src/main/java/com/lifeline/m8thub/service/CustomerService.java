package com.lifeline.m8thub.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lifeline.m8thub.entity.Customer;
import com.lifeline.m8thub.repository.CustomerRepository;
import com.lifeline.m8thub.repository.IRepository;

@Service
@Transactional
public class CustomerService extends AbstractService {
	@Autowired
	private CustomerRepository customerRepository;
	
	@Override
	public IRepository<Customer> getRepository() {
		return customerRepository;
	}
	
	public List<Customer> findAll() {
		return customerRepository.findAll();
	}

	
}
