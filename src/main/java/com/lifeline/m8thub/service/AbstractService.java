package com.lifeline.m8thub.service;

import java.text.ParseException;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.AbstractPersistable;

import com.lifeline.m8thub.exception.ConflictException;
import com.lifeline.m8thub.exception.DuplicatedException;
import com.lifeline.m8thub.exception.NotFoundException;
import com.lifeline.m8thub.repository.IRepository;



public abstract class AbstractService<T extends AbstractPersistable<Long>> {

	/** The message source. */
	@Autowired
	private MessageSource messageSource;
	
	
	/**
	 * Gets the repository.
	 *
	 * @return the repository
	 */
	public abstract IRepository<T> getRepository();

	/**
	 * Find all.
	 *
	 * @return the list
	 */
	public List<T> findAll() {
		return getRepository().findAll();
	}

	/**
	 * Find all.
	 *
	 * @param pageable
	 *            the pageable
	 * @return the page
	 */
	public Page<T> findAll(Pageable pageable) {
		return getRepository().findAll(pageable);
	}

	/**
	 * Find one.
	 *
	 * @param id
	 *            the id
	 * @return the t
	 */
	public T findOne(Long id) {
		return getRepository().findOne(id);
	}

	/**
	 * Delete by primary key.
	 *
	 * @param id            the id
	 * @throws NotFoundException the not found exception
	 */
	public void delete(Long id) throws NotFoundException {
		T element = getRepository().findOne(id);
		if (element == null) {
			throw new NotFoundException();
		}
		getRepository().delete(id);
		
	}
	
	/**
	 * save element
	 * @param element
	 * @return
	 */
	public T save(T element) {
		return getRepository().save(element);
	} 
	
	/**
	 * Save list of entity.
	 *
	 * @param elements            the elements
	 * @return List<T> the list of entity
	 * @throws DuplicatedException the duplicated exception
	 * @throws ParseException the parse exception
	 */
	public List<T> save(List<T> elements) throws DuplicatedException, ParseException {
		return getRepository().save(elements);
	}
	
	/**
	 * Update.
	 *
	 * @param entity the entity
	 * @return the t
	 * @throws NotFoundException the not found exception
	 * @throws DuplicatedException the duplicated exception
	 * @throws ConflictException the conflict exception
	 */
	public T update(T entity) throws 
								NotFoundException, DuplicatedException, ConflictException {
		T existingEntity = getRepository().findOne(entity.getId());
		if (existingEntity == null) {
			throw new NotFoundException();
		}
		BeanUtils.copyProperties(entity, existingEntity);
		getRepository().save(existingEntity);
		return existingEntity;
	}
	
	/**
	 * Count.
	 *
	 * @return the long
	 */
	public long count() {
		return getRepository().count();
	}
	
	/**
	 * Msg.
	 *
	 * @param msgCode the msg code
	 * @param objs the objs
	 * @return the string
	 */
	public String msg(String msgCode, Object... objs) {
		return messageSource.getMessage(msgCode, objs, Locale.US);
	}

	
}